#include<math.h>
#include "matrix.h"
#ifndef ACTIVATION_H 
#define ACTIVATION_H 


//all activation function and their derivitive
//resources https://blog.knoldus.com/activation-function-in-neural-network

//1
double RELU(double num);
double** matrixRELU(int row,int col,double** matrix);
double RELUDev(double num);
double** matrixRELUDev(int row,int col,double** matrix);

//2
double Sigmoid(double num);
double** matrixSigmoid(int row,int col,double** matrix);
double SigmoidDev(double num);
double** matrixSigmoidDev(int row,int col,double** matrix);

//3
double Tanh(double num);
double** matrixTanh(int row,int col,double** matrix);
double TanhDev(double num);
double** matrixTanhDev(int row,int col,double** matrix);

//4
double LeakyRELU(double num);
double** matrixLeakyRELU(int row,int col,double** matrix);
double LeakyRELUDev(double num);
double** matrixLeakyRELUDev(int row,int col,double** matrix);

//5
double ParamRELU(double num,double param);
double** matrixParamRELU(int row,int col,double** matrix,double param);
double ParamRELUDev(double num,double param);
double** matrixParamRELUDev(int row,int col,double** matrix,double param);

//6
double ELU(double num,double param);
double** matrixELU(int row,int col,double** matrix,double param);
double ELUDev(double num,double param);
double** matrixELUDev(int row,int col,double** matrix,double param);

//7
double** matrixSoftMax(int row,int col,double** matrix);
double** matrixSoftMaxDev(int row,int col,double** matrix);

//8
double Swish(double num,double param);
double** matrixSwish(int row,int col,double** matrix,double param);
double SwishDev(double num,double param);
double** matrixSwishDev(int row,int col,double** matrix,double param);

//9
double GELU(double num);
double** matrixGELU(int row,int col,double** matrix);
double GELUDev(double num);
double** matrixGELUDev(int row,int col,double** matrix);

//10
double SELU(double num);
double** matrixSELU(int row,int col,double** matrix); 
double SELUDev(double num);
double** matrixSELUDev(int row,int col,double** matrix); 

//SELU Function
double SELU(double num){ double long h = 1.0507009873554804934193349852946;double long a = 1.6732632423543772848170429916717; return num > 0 ? num*h : (a*h)*(pow(M_E,num)-1); }

double** matrixSELU(int row,int col,double** matrix){
	double** matrixRes = matrix2D(row,col);
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			matrixRes[i][j] = SELU(matrix[i][j]);
	return matrixRes;
}

double SELUDev(double num){ double long h = 1.0507009873554804934193349852946;double long a = 1.6732632423543772848170429916717; return num > 0 ? h : a*h*pow(M_E,num); }

double** matrixSELUDev(int row,int col,double** matrix){
	double** matrixRes = matrix2D(row,col);
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			matrixRes[i][j] = SELUDev(matrix[i][j]);
	return matrixRes;
}


//GELU Function
double GELU(double num){ return (0.5*num)*(1+Tanh(sqrt(2/M_PI)*(num+(0.044715*pow(num,3))))); }

double** matrixGELU(int row,int col,double** matrix){
	double** matrixRes = matrix2D(row,col);
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			matrixRes[i][j] = GELU(matrix[i][j]);
	return matrixRes;
}

double GELUDev(double num){ return 0.5*Tanh(0.0356774*pow(num,3)+0.797885*num)+(0.0535161*pow(num,3)+0.398942*num)*TanhDev(0.0356774*pow(num,3)+0.797885*num)+0.5; }

double** matrixGELUDev(int row,int col,double** matrix){
	double** matrixRes = matrix2D(row,col);
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			matrixRes[i][j] = GELUDev(matrix[i][j]);
	return matrixRes;
}

//Swish Function 
double Swish(double num,double param){ return num*Sigmoid(num*param); }

double** matrixSwish(int row,int col,double** matrix,double param){
	double** matrixRes = matrix2D(row,col);
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			matrixRes[i][j] = Swish(matrix[i][j],param);
	return matrixRes;
}

double SwishDev(double num,double param){ double sw = Swish(num,param); return (sw*param)+(Sigmoid(param*num)*(1-(sw*param))); }

double** matrixSwishDev(int row,int col,double** matrix,double param){
	double** matrixRes = matrix2D(row,col);
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			matrixRes[i][j] = SwishDev(matrix[i][j],param);
	return matrixRes;
}


//softMax function
double** matrixSoftMax(int row,int col,double** matrix){
	double sum = 0;
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			sum+=pow(M_E,matrix[i][j]);
	double** matrixRes = matrix2D(row,col);
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			matrixRes[i][j] = pow(M_E,matrix[i][j])/sum;
	return matrixRes;
}

double** matrixSoftMaxDev(int row,int col,double** matrix){
	double sum = 0;
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			sum+=pow(M_E,matrix[i][j]);
	double** matrixRes = matrix2D(row,col);
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			matrixRes[i][j] = pow(M_E,matrix[i][j])/sum;
	return matrixRes;
}

//ELU function
double ELU(double num,double param){ return num >= 0 ? num : param*(pow(M_E,num)-1); }

double** matrixELU(int row,int col,double** matrix,double param){
	double** matrixRes = matrix2D(row,col);
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			matrixRes[i][j] = ELU(matrix[i][j],param);
	return matrixRes;
}

double ELUDev(double num,double param){ return num >= 0 ? num : ELU(num,param)+param; }

double** matrixELUDev(int row,int col,double** matrix,double param){
	double** matrixRes = matrix2D(row,col);
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			matrixRes[i][j] = ELUDev(matrix[i][j],param);
	return matrixRes;
}

//ParamRELU function
double ParamRELU(double num,double param){ return num >=0 ? num : num*param; }

double** matrixParamRELU(int row,int col,double** matrix,double param){
	double** matrixRes = matrix2D(row,col);
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			matrixRes[i][j] = ParamRELU(matrix[i][j],param);
	return matrixRes;
}

double ParamRELUDev(double num,double param){ return num >=0 ? 1 : param; }

double** matrixParamRELUDev(int row,int col,double** matrix,double param){
	double** matrixRes = matrix2D(row,col);
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			matrixRes[i][j] = ParamRELUDev(matrix[i][j],param);
	return matrixRes;
}

//LeakyRELU function
double LeakyRELU(double num){ return num >= 0 ? num : num*0.1; }

double** matrixLeakyRELU(int row,int col,double** matrix){
	double** matrixRes = matrix2D(row,col);
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			matrixRes[i][j] = LeakyRELU(matrix[i][j]);
	return matrixRes;
}

double LeakyRELUDev(double num){ return num >= 0 ? 1 : 0.01; }

double** matrixLeakyRELUDev(int row,int col,double** matrix){
	double** matrixRes = matrix2D(row,col);
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			matrixRes[i][j] = LeakyRELUDev(matrix[i][j]);
	return matrixRes;
}

//Tanh Function
double Tanh(double num){ return (pow(M_E,num)-pow(M_E,-num))/(pow(M_E,num)+pow(M_E,-num)); }

double** matrixTanh(int row,int col,double** matrix){
	double** matrixRes = matrix2D(row,col);
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			matrixRes[i][j] = Tanh(matrix[i][j]);
	return matrixRes;
}

double TanhDev(double num){ return 4/(pow(pow(M_E,-num)+pow(M_E,num),2));}

double** matrixTanhDev(int row,int col,double** matrix){
	double** matrixRes = matrix2D(row,col);
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			matrixRes[i][j] = TanhDev(matrix[i][j]);
	return matrixRes;
}

//Sigmoid Function
double Sigmoid(double num){ return 1/(1+pow(M_E,-num)); }

double** matrixSigmoid(int row,int col,double** matrix){
	double** matrixRes = matrix2D(row,col);
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			matrixRes[i][j] = Sigmoid(matrix[i][j]);
	return matrixRes;
}

double SigmoidDev(double num){ double buff = Sigmoid(num); return num*(1-num); }

double** matrixSigmoidDev(int row,int col,double** matrix){
	double** matrixRes = matrix2D(row,col);
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			matrixRes[i][j] = SigmoidDev(matrix[i][j]);
	return matrixRes;
}

//RELU function
double RELU(double num){ return num >= 0 ? num : 0;}

double** matrixRELU(int row,int col,double** matrix){
	double** matrixRes = matrix2D(row,col);
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			matrixRes[i][j] = RELU(matrix[i][j]);
	return matrixRes;
}

double RELUDev(double num){ return num >= 0 ? 1 : 0;}

double** matrixRELUDev(int row,int col,double** matrix){
	double** matrixRes = matrix2D(row,col);
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			matrixRes[i][j] = RELUDev(matrix[i][j]);
	return matrixRes;
}
#endif
