#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#ifndef MATRIX_H
#define MATRIX_H

//returns the copy of a matrix
double** matrixCopy(int row,int col,double** matrix);

//sets all elements of a matrix to num
void matrixSet(int row,int col,double** matrix,double num);

//preformes matrix2-=matrix1
void matrixSubtractionEqual(int row,int col,double** matrix1,double** matrix2,int f1,int f2);

//preformes matrix2+=matrix1
void matrixAdditionEqual(int row,int col,double** matrix1,double** matrix2,int f1,int f2);

//returns the transpose of the matrix
double** matrixTranspose(int row,int col,double** matrix);

//frees a matrix
void matrixFree(int row,int col,double** matrix);

//display a 2d matrix
void matrixDisplay(int row,int col,double** table);

//allocate memory for a 2D matrix 
double** matrix2D(int row,int col);

//adds 2 2D matricies
double** matrixAddition(int row,int col,double** matrix1,double** matrix2,int f1,int f2);

//subtracts matrix 1 to matrix 2
double** matrixSubtraction(int row,int col,double** matrix1,double** matrix2,int f1,int f2);

//returns a random double number
double randomDouble(double min, double max);

//returns a matrix with random values between -1 and 1
double** matrixMake(int row,int col);

//multiplys 2 matrices return the resault
double** matrixMultiply(int rows,int colums,double** Matrix,int rows1,int colums1,double** Matrix1,int f1,int f2);

//normalizes the matrix
void matrixNormalize(int row,int col,double** matrix);

//returns the sum of the matrix
double matrixSum(int row,int col,double** matrix);

//multiplys all matrix elements by a num
void matrixMultiplyScalar(int row,int col,double** matrix,double num);

//preformes hamard matrix multiplication
double** matrixHamard(int row,int col,double** matrix,double** matrix1,int f1,int f2);

//returns index of max element in a vector
int matrixMaxIndex(int row,int col,double** matrix);

void matrixDisplay(int row,int col,double** table){
	for(int i=0;i<row;i++){
		for(int j =0;j<col;j++)
			printf("%f ",table[i][j]);
		printf("\n");
	}
}

double** matrix2D(int row,int col){
	double** table = calloc(row,sizeof(double*));
	for(int i=0;i<row;i++)
		table[i] = calloc(col,sizeof(double));
	return table;
}

double** matrixAddition(int row,int col,double** matrix1,double** matrix2,int f1,int f2){
	double** matrixRes = matrix2D(row,col);
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			matrixRes[i][j] = matrix1[i][j]+matrix2[i][j];
	if(f1)
		matrixFree(row,col,matrix1);
	if(f2)
		matrixFree(row,col,matrix2);
	return matrixRes;
}

void matrixAdditionEqual(int row,int col,double** matrix1,double** matrix2,int f1,int f2){
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			matrix2[i][j]+=matrix1[i][j];
	if(f1)
		matrixFree(row,col,matrix1);
	if(f2)
		matrixFree(row,col,matrix2);
}

double** matrixSubtraction(int row,int col,double** matrix1,double** matrix2,int f1,int f2){
	double** matrixRes = matrix2D(row,col);
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			matrixRes[i][j] = matrix1[i][j]-matrix2[i][j];
	if(f1)
		matrixFree(row,col,matrix1);
	if(f2)
		matrixFree(row,col,matrix2);
	return matrixRes;
}

void matrixSubtractionEqual(int row,int col,double** matrix1,double** matrix2,int f1,int f2){
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			matrix2[i][j]-=matrix1[i][j];
	if(f1)
		matrixFree(row,col,matrix1);
	if(f2)
		matrixFree(row,col,matrix2);
}

double** matrixMake(int row,int col){
	double** matrixRes = matrix2D(row,col);
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			matrixRes[i][j] = randomDouble(-1,1);
	return matrixRes;
}

double** matrixMultiply(int rows,int colums,double** Matrix,int rows1,int colums1,double** Matrix1,int f1,int f2){
	double** MatrixRes = matrix2D(rows,colums1);
	for(int i=0;i<rows;i++)
		for(int j=0;j<colums1;j++)
			for(int k=0;k<colums;k++)
				MatrixRes[i][j] += Matrix[i][k]*Matrix1[k][j];
	if(f1)
		matrixFree(rows,colums,Matrix);
	if(f2)
		matrixFree(rows1,colums1,Matrix1);
	return MatrixRes;
}

double randomDouble(double min, double max){
	return min + (rand() / (RAND_MAX/(max-min)));
}

void matrixFree(int row,int col,double** matrix){
	for(int i=0;i<row;i++)
		free(matrix[i]);
	free(matrix);
}

double matrixSum(int row,int col,double** matrix){
	double sum = 0;
	for(int i =0;i<row;i++)
		for(int j=0;j<col;j++)
			sum+=matrix[i][j];
	return sum;
}

void matrixNormalize(int row,int col,double** matrix){
	double sum = matrixSum(row,col,matrix);
	sum/=(row*col);
	for(int i =0;i<row;i++)
		for(int j=0;j<col;j++)
			matrix[i][j]-=sum;
}

void matrixMultiplyScalar(int row,int col,double** matrix,double num){
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			matrix[i][j]*=num;
}

double** matrixHamard(int row,int col,double** matrix,double** matrix1,int f1,int f2){
	double** matrixRes = matrix2D(row,col);
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			matrixRes[i][j] = matrix[i][j]*matrix1[i][j];
	if(f1)
		matrixFree(row,col,matrix);
	if(f2)
		matrixFree(row,col,matrix1);
	return matrixRes;
}

double** matrixCopy(int row,int col,double** matrix){
	double** matrixRes = matrix2D(row,col);
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			matrixRes[i][j] = matrix[i][j];
	return matrixRes;
}

void matrixSet(int row,int col,double** matrix,double num){
	for(int i=0;i<row;i++)
		for(int j=0;j<col;j++)
			matrix[i][j] = num;
}

double** matrixTranspose(int row,int col,double** matrix){
	double** MatrixRes = matrix2D(col,row);
    for(int i=0;i<col;i++){
        for(int j=0;j<row;j++){
            MatrixRes[i][j] = matrix[j][i];
        }
    }
	return MatrixRes;
}

int matrixMaxIndex(int row,int col,double** matrix){
	int max =0;
	for(int i=0;i<row;i++)
		if(matrix[max][0] < matrix[i][0])
			max = i;
	return max;
}
#endif
