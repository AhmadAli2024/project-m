#include<math.h>
#include"matrix.h"
#include"activation.h"
#ifndef NETWORK_H 
#define NETWORK_H 


//all network functions


struct Network{
	int numLayers;
	int* sizes;
	int* activations;
	double*** matrixN;
	double*** matrixNRELU;
	double*** matrixW;
	double*** matrixB;
	double*** matrixCW;
	double*** matrixCB;
};
//preformes the activation function specified by the type
double** matrixActivationDev(int row,int col,double** matrix,int type,double param);

//preformes the activation Dev function specified by the type
double** matrixActivation(int row,int col,double** matrix,int type,double param);
//imports the networks weights and biases from a .txt file
void netImport(struct Network* net);

//exports the networks weights and biases onto a .txt file
void netExport(struct Network* net);

//sets the weights to the H2 weight convnetion
void matrixH2WB(struct Network* net);

//declares the weights and biases of the network
void beginNet(struct Network* net);

//does the forward loop of the neural net
void Forward(struct Network* net);

//frees all data collected from the Forward pass
void resetNet(struct Network* net);

//frees all weight and biases data from network 
void freeNet(struct Network* net);

//changes the weight and biases
void Backprop(struct Network* net,double** target);


void matrixH2WB(struct Network* net){
	for(int i=0;i<net->numLayers-1;i++)
		matrixMultiplyScalar(net->sizes[i+1],net->sizes[i],net->matrixW[i],sqrt((double)2/net->sizes[i]));
}

void beginNet(struct Network* net){
	net->matrixN = calloc(net->numLayers,sizeof(double**));
	net->matrixNRELU = calloc(net->numLayers,sizeof(double**));
	net->matrixB = calloc(net->numLayers-1,sizeof(double**));
	net->matrixW = calloc(net->numLayers-1,sizeof(double**));
	net->matrixCW = calloc(net->numLayers-1,sizeof(double**));
	net->matrixCB = calloc(net->numLayers-1,sizeof(double**));
	for(int i=0;i<net->numLayers-1;i++){
		net->matrixB[i] = matrixMake(net->sizes[i+1],1);
		net->matrixW[i] = matrixMake(net->sizes[i+1],net->sizes[i]);
		net->matrixCW[i] = matrix2D(net->sizes[i+1],net->sizes[i]);
		net->matrixCB[i] = matrix2D(net->sizes[i+1],1);
	}
}

void resetNet(struct Network* net){
	for(int i=0;i<net->numLayers;i++){
		matrixFree(net->sizes[i],1,net->matrixN[i]);
		matrixFree(net->sizes[i],1,net->matrixNRELU[i]);
	}
}
		
void freeNet(struct Network* net){
	for(int i=0;i<net->numLayers-1;i++){
		matrixFree(net->sizes[i+1],1,net->matrixB[i]);
		matrixFree(net->sizes[i+1],net->sizes[i],net->matrixW[i]);
		matrixFree(net->sizes[i+1],net->sizes[i],net->matrixCW[i]);
		matrixFree(net->sizes[i+1],1,net->matrixCB[i]);
	}
	free(net->matrixCW);
	free(net->matrixCB);
	free(net->matrixB);
	free(net->matrixW);
	free(net->matrixN);
	free(net->matrixNRELU);
	free(net->sizes);
}

void Forward(struct Network* net){
	for(int i=0;i<net->numLayers-1;i++){
		net->matrixN[i+1] = matrixAddition(net->sizes[i+1],1,matrixMultiply(net->sizes[i+1],net->sizes[i],net->matrixW[i],net->sizes[i],1,net->matrixNRELU[i],0,0),net->matrixB[i],1,0);
		net->matrixNRELU[i+1] = matrixActivation(net->sizes[i+1],1,net->matrixN[i+1],net->activations[i],0);
	}
}

void Backprop(struct Network* net,double** targetV){
	double*** layerLoses = calloc(net->numLayers-1,sizeof(double**));
	//calculating error in the last layer( C hamard zL) 
	double** lossFunction = matrixSubtraction(net->sizes[net->numLayers-1],1,net->matrixNRELU[net->numLayers-1],targetV,0,1);
	layerLoses[net->numLayers-2] = matrixHamard(net->sizes[net->numLayers-1],1,lossFunction,matrixActivationDev(net->sizes[net->numLayers-1],1,net->matrixN[net->numLayers-1],net->activations[net->numLayers-2],0),1,1);

	//calculating the error for the other layers
	for(int i=net->numLayers-3;i>=0;i--)
		layerLoses[i] = matrixHamard(net->sizes[i+1],1,matrixMultiply(net->sizes[i+1],net->sizes[i+2],matrixTranspose(net->sizes[i+2],net->sizes[i+1],net->matrixW[i+1]),net->sizes[i+2],1,layerLoses[i+1],1,0),matrixActivationDev(net->sizes[i+1],1,net->matrixN[i+1],net->activations[i],0),1,1);

	//calculating the slope for schastic gradient decent
	for(int i=0;i<net->numLayers-1;i++){
		matrixAdditionEqual(net->sizes[i+1],net->sizes[i],matrixMultiply(net->sizes[i+1],1,layerLoses[i],1,net->sizes[i],matrixTranspose(net->sizes[i],1,net->matrixNRELU[i]),0,1),net->matrixCW[i],1,0);
		matrixAdditionEqual(net->sizes[i+1],1,layerLoses[i],net->matrixCB[i],0,0);
	}

	//freeing the losses errors in the layerLoss array
	for(int i=0;i<net->numLayers-1;i++)
		matrixFree(net->sizes[i+1],1,layerLoses[i]);
	free(layerLoses);
}

void netExport(struct Network* net){
	FILE* input;
	input = fopen("export.txt","w");
	for(int i=0;i<net->numLayers-1;i++){
		for(int j=0;j<net->sizes[i+1];j++)
			for(int k=0;k<net->sizes[i];k++)
				fprintf(input,"%f ",net->matrixW[i][j][k]);
		fprintf(input,"\n");
		for(int j=0;j<net->sizes[i+1];j++)
			fprintf(input,"%f ",net->matrixB[i][j][0]);
	}
	fclose(input);
}

void netImport(struct Network* net){
	FILE* input;
	input = fopen("export.txt","r");
	for(int i=0;i<net->numLayers-1;i++){
		for(int j=0;j<net->sizes[i+1];j++)
			for(int k=0;k<net->sizes[i];k++)
				fscanf(input,"%le ",&net->matrixW[i][j][k]);
		fscanf(input,"\n");
		for(int j=0;j<net->sizes[i+1];j++)
			fscanf(input,"%le ",&net->matrixB[i][j][0]);
	}
	fclose(input);
}

double** matrixActivation(int row,int col,double** matrix,int type,double param){
	switch(type){
		case 1:
			return matrixRELU(row,col,matrix);
		case 2:
			return matrixSigmoid(row,col,matrix);
		case 3:
			return matrixTanh(row,col,matrix);
		case 4:
			return matrixLeakyRELU(row,col,matrix);
		case 5:
			return matrixParamRELU(row,col,matrix,param);
		case 6:
			return matrixELU(row,col,matrix,param);
		case 7:
			return matrixSoftMax(row,col,matrix);
		case 8:
			return matrixSwish(row,col,matrix,param);
		case 9:
			return matrixGELU(row,col,matrix);
		case 10:
			return matrixSELU(row,col,matrix);
		default:
			return matrixCopy(row,col,matrix);
	}
}

double** matrixActivationDev(int row,int col,double** matrix,int type,double param){
	switch(type){
		case 1:
			return matrixRELUDev(row,col,matrix);
		case 2:
			return matrixSigmoidDev(row,col,matrix);
		case 3:
			return matrixTanhDev(row,col,matrix);
		case 4:
			return matrixLeakyRELUDev(row,col,matrix);
		case 5:
			return matrixParamRELUDev(row,col,matrix,param);
		case 6:
			return matrixELUDev(row,col,matrix,param);
		case 7:
			return matrixSoftMaxDev(row,col,matrix);
		case 8:
			return matrixSwishDev(row,col,matrix,param);
		case 9:
			return matrixGELUDev(row,col,matrix);
		case 10:
			return matrixSELUDev(row,col,matrix);
		default:
			return matrixCopy(row,col,matrix);
	}
}



#endif
